Since 2001, Mark's Electric has been providing dependable electrical contracting and remodeling services to residential and commercial locations in Central PA. We offer rewiring, new construction, service upgrades, bathroom & kitchen remodeling, painting, and deck/patios.

Address: 465 2nd St, Montandon, PA 17850

Phone: 570-758-3656